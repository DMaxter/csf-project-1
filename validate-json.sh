#!/bin/bash

arg=$1

if [ -z "$arg" ]; then
    arg="./reports/*.json"
fi

payload=""
subdir=""
inpath=""
for file in $arg
do
    subdir=$(dirname $file)
    inpath=$(realpath $subdir)
    payload="${payload}-d $(basename $file) "
done

docker run -v $inpath:/file pajv -s /schema/report-schema.json $payload
